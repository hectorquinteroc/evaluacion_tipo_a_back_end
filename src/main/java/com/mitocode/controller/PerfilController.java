package com.mitocode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Rol;
import com.mitocode.model.Usuario;
import com.mitocode.service.IPerfilService;

@RestController
@RequestMapping("/perfil")
public class PerfilController {

	@Autowired
	private IPerfilService iPerfilService;
	
//	@PostMapping("/user")
//	public ResponseEntity<Rol> getRolesUsuario(@RequestBody String usuario) throws Exception{
//		List<Rol> user = iPerfilService.getRolesUsuario(usuario);
//		return new ResponseEntity<Rol>(user.get(0), HttpStatus.OK);
//	}
	
	@PostMapping("/{user}")
	public ResponseEntity<Usuario> getRolesUsuario(@PathVariable("user") String user) throws Exception{
		Usuario usuario = iPerfilService.getRolesUsuario(user);
		
		if(usuario == null) {
			throw new ModeloNotFoundException("USUARIO NO ENCONTRADO: " + user);
		}
		System.out.println("USUARIO ENCONTRADO: " + usuario.getUsername() + " ROL ENCONTRADO " + usuario.getRoles().get(0).getNombre());
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}
}
