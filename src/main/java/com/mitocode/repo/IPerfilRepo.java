package com.mitocode.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.Usuario;

public interface IPerfilRepo extends IGenericRepo<Usuario, Integer>{

	@Query("FROM Usuario us where us.username =:usuario")
	Usuario getRolesUsuario(@Param("usuario")String usuario);
}
