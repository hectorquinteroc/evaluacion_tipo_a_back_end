package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.model.Usuario;
import com.mitocode.repo.IPerfilRepo;
import com.mitocode.service.IPerfilService;

@Service
public class PerfilServiceImpl implements IPerfilService{
	
	@Autowired
	IPerfilRepo iPerfilRepo;
	
//	@Override
//	public List<Rol> getRolesUsuario(String usuario) {
//		System.out.println("PerfilServiceImpl " + iPerfilRepo.getRolesUsuario(usuario));
//		return iPerfilRepo.getRolesUsuario(usuario).getRoles();
//	}
	
	@Override
	public Usuario getRolesUsuario(String usuario) {
		return iPerfilRepo.getRolesUsuario(usuario);
	}
}
